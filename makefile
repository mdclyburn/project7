CC = g++
ARGS =
OUT = pdbparser

$(OUT): main.o PDBParser.o Atom.o
	$(CC) $(ARGS) main.o PDBParser.o Atom.o -o $(OUT)

main.o: main.cpp
	$(CC) $(ARGS) -c main.cpp
PDBParser.o: PDBParser.cpp
	$(CC) $(ARGS) -c PDBParser.cpp
Atom.o: Atom.cpp
	$(CC) $(ARGS) -c Atom.cpp

clean:
	rm -rf *.o $(OUT)
