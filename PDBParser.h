#ifndef PDBPARSER_H
#define PDBPARSER_H

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>

#include "Atom.h"

class PDBParser
{
public:
	PDBParser();
	~PDBParser();
	
	void parse(char* filename);
	std::vector<Atom> getAtoms();
	
	Atom getAtom(int index);
	Atom getAtom(std::string ref, int res);
	
	float findVector(int i);
protected:
private:
	std::vector<Atom> atoms;
	
	bool is_number(std::string s)
	{
		std::string::const_iterator it = s.begin();
		while (it != s.end() && std::isdigit(*it)) ++it;
		return !s.empty() && it == s.end();
	}

	bool contains(std::string a, std::string b)
	{
		if(std::string::npos != a.find(b))
			return true;
		return false;
	}
};

#endif