#include <iostream>
#include <fstream>
#include "PDBParser.h"
#include "Atom.h"
using namespace std;

int main(int argc, char* args[])
{
	if(argc != 6)
	{
		cout << "Error. Invalid arguments." << endl;
		return -1;
	}
	
	PDBParser parser = PDBParser();
	parser.parse(args[1]);
	
	// Find the atom and get the coordinates.
	Atom a = parser.getAtom(args[3], atoi(args[4]));
	
	ofstream ofs;
	ofs.open(args[2]);
	
	for(int i = 0; i < parser.getAtoms().size(); i++)
	{
		if(parser.findVector(i) <= atof(args[5]))
		{
			ofs << parser.getAtom(i).getRaw() << endl;
			cout << parser.getAtom(i).getRaw() << endl;
		}
	}
	
	ofs.close();
		
	return 0;
}

