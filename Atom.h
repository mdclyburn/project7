/* 
 * File:   Atom.h
 * Author: marshall
 *
 * Created on April 25, 2013, 11:06 PM
 */

#ifndef ATOM_H
#define	ATOM_H

#include <string>

class Atom {
public:
    Atom();
    Atom(int no, std::string name, int residue, float x, float y, float z);
    virtual ~Atom();
    
    int getNo() const;
    std::string getName() const;
    int getRes() const;
    float getx() const;
    float gety() const;
    float getz() const;
	std::string getRaw();
	
	void setRaw(std::string raw);
	
	Atom& operator=(const Atom& a)
	{
		if(this == &a)
			return *this;
		no = a.getNo();
		name = a.getName();
		residue = a.getRes();
		x = a.getx();
		y = a.gety();
		z = a.getz();
		
		return *this;
	}
    
private:
    int no;
    std::string name;
    int residue;
    float x;
    float y;
    float z;
	std::string raw;
};

#endif	/* ATOM_H */

