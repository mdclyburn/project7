#include "PDBParser.h"

PDBParser::PDBParser()
{
	// ctor
}

PDBParser::~PDBParser()
{
	// dtor
}

void PDBParser::parse(char* filename)
{
	// Open our file for streaming.
	std::ifstream ifs;
	ifs.open(filename);
	
	std::vector<std::string> tokens; // will hold the tokens
	tokens.reserve(475); // go ahead and give it a heads up as to its capacity
	std::string token; // holds the token the program is currently working with
	int count = 0; // will count the number of valid atom tokens
	
	while(getline(ifs, token, ' '))
	{
		if(token.size() > 0)
			tokens.push_back(token);
	}
	
	for(int i = 0; i < tokens.size(); i++)
	{
		if(contains(tokens[i], "ATOM") && is_number(tokens[i + 1]))
		{
			// gather data
			int no = atoi(tokens[i + 1].c_str());
			std::string name = tokens[i + 2];
			int ref = atoi(tokens[i + 5].c_str());
			float x = atof(tokens[i + 6].c_str());
			float y = atof(tokens[i + 7].c_str());
			float z = atof(tokens[i + 8].c_str());
			
			atoms.push_back(Atom(no, name, ref, x, y, z));
			count++;
		}
	}
	
	ifs.close();
	ifs.open(filename);
	int i = 0;
	
	while(getline(ifs, token) && i < atoms.size())
	{
		if(std::string::npos != token.substr(0, 4).find("ATOM"))
		{
			atoms[i].setRaw(token);
			i++;
		}
	}
	
	std::cout << "Found " << count << " atoms entries in " << filename << "." << std::endl;
}

std::vector<Atom> PDBParser::getAtoms()
{
	return atoms;
}

Atom PDBParser::getAtom(int index)
{
	if(index < 0)
		return atoms[0];
	if(index > atoms.size() - 1)
		return atoms[atoms.size() - 1];
	return atoms[index];
}

Atom PDBParser::getAtom(std::string name, int res)
{
	for(int i = 0; i < atoms.size(); i++)
	{
		if(atoms[i].getRes() == res && atoms[i].getName().compare(name) == 0)
			return atoms[i];
	}
	
	return Atom();
}

float PDBParser::findVector(int i)
{
	return (sqrt(pow(atoms[i].getx(), 2) +
				pow(atoms[i].gety(), 2) +
				pow(atoms[i].getz(), 2)));
}
