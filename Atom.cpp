#include "Atom.h"

Atom::Atom()
{
    this->no = -1;
    this->name = "ERR";
    this->residue = -1;
    this->x = 0;
    this->y = 0;
    this->z = 0;
}

Atom::Atom(int no, std::string name,
            int residue, float x, float y, float z)
{
    this->no = no;
    this->name = name;
    this->residue = residue;
    this->x = x;
    this->y = y;
    this->z = z;
}

Atom::~Atom()
{
}

int Atom::getNo() const
{
	return no;
}

std::string Atom::getName() const
{
	return name;
}

int Atom::getRes() const
{
	return residue;
}

float Atom::getx() const
{
	return x;
}

float Atom::gety() const
{
	return y;
}

float Atom::getz() const
{
	return z;
}

std::string Atom::getRaw()
{
	return raw;
}

void Atom::setRaw(std::string raw)
{
	this->raw = raw;
	return;
}
